#Written by Matteo M. Fusi, Polimi

#dirs
ARCH_FP_DIR="ArchFP"
TSP_DIR="TSPcomputation-1.1"
HOTSPOT_DIR="HotSpot-6.0"
BUILD_DIR="build"
BIN_DIR="bin"
TRACE_DIR="traces"

#names of the files
ARCH_FP_OUT_NAME="Nehalem5.flp"
ARCH_FP_EXEC_NAME="ArchFP"
TSP_OUT_NAME="tsp_out"
TSP_EXEC_NAME="TSPcomputation"
HOTSPOT_EXEC_NAME="hotspot"
HOTSPOT_FP_EXEC_NAME="hotfloorplan"
HOTSPOT_FP_OUT_NAME="hotfloor.out"
HOTSPOT_OUT_NAME="hotspot.out"
HOTSPOT_TRACE_NAME="power_trace_32_cores.ptrace"
HOTSPOT_STDOUT_NAME="hs_multicore.out"
TSP_TRACE_NAME="power_trace_TSP.ptrace"

#simplify: concatenate variables
#executables
ARCH_FP_EXEC=$(BIN_DIR)/$(ARCH_FP_EXEC_NAME)
HOTSPOT_EXEC=$(BIN_DIR)/$(HOTSPOT_EXEC_NAME)
HOTSPOT_FP_EXEC=$(BIN_DIR)/$(HOTSPOT_FP_EXEC_NAME)
TSP_EXEC=$(BIN_DIR)/$(TSP_EXEC_NAME)
#files
ARCH_FP_OUT=$(BUILD_DIR)/$(ARCH_FP_OUT_NAME)
HOTSPOT_OUT=$(BUILD_DIR)/$(HOTSPOT_OUT_NAME)
HOTSPOT_TRACE=$(TRACE_DIR)/$(HOTSPOT_TRACE_NAME)
HOTSPOT_STDOUT=$(BUILD_DIR)/$(HOTSPOT_STDOUT_NAME)
TSP_EXEC=$(BIN_DIR)/$(TSP_EXEC_NAME)
TSP_TRACE=$(TRACE_DIR)/$(TSP_TRACE_NAME)

#TSP params. EDIT THESE
N_CORES=32
N_BLOCKS=64
T_DTM=80
T_AMB=45
P_MAX=250
P_INACTIVE=0.14

TSP_PARAMS=-cores $(N_CORES) -n_blocks $(N_BLOCKS) -t_amb $(T_AMB) -t_dtm $(T_DTM) -p_max $(P_MAX) -p_inactive_core $(P_INACTIVE) -p_blocks $(TSP_TRACE) -b $(HOTSPOT_STDOUT) -g $(HOTSPOT_STDOUT)

#append tspmap if it's defined
ifdef TSP_MAP
TSP_PARAMS+=-mapping $(TSP_MAP)
endif

all: clean tsp

#launch ArchFP and move result in
ArchFP: $(ARCH_FP_EXEC)
	$(ARCH_FP_EXEC)
	mv -f $(ARCH_FP_OUT_NAME) $(ARCH_FP_OUT)

#make ArchFP
$(ARCH_FP_EXEC):
	make -C $(ARCH_FP_DIR)
	mv -f $(ARCH_FP_DIR)/$(ARCH_FP_EXEC_NAME) $(ARCH_FP_EXEC)

#make hotspot (also builds hotfloorplan)
makeHotspot:
	make -C $(HOTSPOT_DIR)
	mv -f $(HOTSPOT_DIR)/$(HOTSPOT_EXEC_NAME) $(HOTSPOT_EXEC)
	mv -f $(HOTSPOT_DIR)/$(HOTSPOT_FP_EXEC_NAME) $(HOTSPOT_FP_EXEC)

hotspot: makeHotspot ArchFP
	$(HOTSPOT_EXEC) -f $(ARCH_FP_OUT) -p $(HOTSPOT_TRACE) -model_type block -o $(HOTSPOT_OUT) > $(HOTSPOT_STDOUT)

#rule for TSP executable
$(TSP_EXEC):
	cd $(TSP_DIR) && bash compileTSP && cd ..
	mv -f $(TSP_DIR)/$(TSP_EXEC_NAME) $(TSP_EXEC)

#rule that executes TSP
tsp: $(TSP_EXEC) hotspot
	$(TSP_EXEC) $(TSP_PARAMS)

clean:
	rm -r $(BUILD_DIR)/*

uninstall:
	rm -r $(BIN_DIR)/*

remove:
	rm -r -d $(BIN_DIR)
	rm -r -d $(BUILD_DIR)


#draw ArchFP scheme
scheme: ArchFP
	cd $(ARCH_FP_DIR) && bash hs2pdf ../$(ARCH_FP_OUT) && cd ..

hotScheme: hotspot
	perl $(HOTSPOT_DIR)/grid_thermal_map.pl build/archFP_alt.flp traces/tempSch.t 8 8 > $(BUILD_DIR)/hotScheme.svg
