void generateNehalem_22nm()
{
	// Areas from McPAT.
	double rz_icache  = 0.62914;
	double rz_dcache  = 2.4629;
	double rz_noc     = 0.130198;
	double rz_l2     = 1.05182;
	double rz_logic   = 9.06246; // - rz_icache - rz_dcache;
	double rz_pcaches = rz_dcache + rz_icache + rz_l2;
    double rz_l3      = 15.5293/4;
	
	cout << "Nehalem Logic area:\t" << rz_logic << "\n";
	cout << "L3 area:\t" << rz_l3 << "\n";
	
    double rpoverhead = 1.1;
    rz_dcache  *= rpoverhead;
	
    double undiff_ratio = 1.20496;
    rz_icache  *= undiff_ratio;
    rz_dcache  *= undiff_ratio;
    rz_logic   *= undiff_ratio;
	rz_pcaches *= undiff_ratio;
	rz_noc	   *= undiff_ratio;
	rz_l2	   *= undiff_ratio;
	rz_l3	   *= undiff_ratio;
	
    /*
    geogLayout * Cache = new geogLayout();
    Cache->addComponentCluster("NoC", 1, rz_noc, 20., 1., Left);
    Cache->addComponentCluster("P$", 1, rz_pcaches, 20., 1., Right);
	
	
	//Caches+Logic
	geogLayout * core1 = new geogLayout();
	core1->addComponentCluster("Logic", 1, rz_logic, 20., 1., Top);
	core1->addComponent(Cache, 1, Center);
	core1->addComponentCluster("LL$", 1, rz_l3, 20., 1., Bottom);

	geogLayout * core2 = new geogLayout();
	core2->addComponentCluster("LL$", 1, rz_l3, 20., 1., Top);
	core2->addComponent(Cache, 1, Center);
	core2->addComponentCluster("Logic", 1, rz_logic, 20., 1., Bottom);
	
	geogLayout * core = new geogLayout();
	core->addComponent(core1, 1, Top);
	core->addComponent(core2, 1, Bottom);
	//core->addComponent(Cache, 1, Bottom);
	//core->addComponentCluster("Logic", 1, rz_logic, 20., 1., Bottom);
	*/
	
	
	geogLayout * coreCluster = new geogLayout();
	coreCluster->addComponentCluster("core_", 1, rz_logic, 20., 1., Top);
	coreCluster->addComponentCluster("L2_", 1, rz_l2, 20., 1., Bottom);

	gridLayout * chip = new gridLayout(); 
  	chip->addComponent(coreCluster, 32);

	/*
	geogLayout * chip = new geogLayout();
	chip->addComponent(coreCluster, 32, Top);
	*/
	
    bool success = chip->layout(AspectRatio, 1);
    if (!success) cout << "Unable to layout specified CMP configuration.";
    else 
    {
        setNameMode(true);
        ostream& HSOut = outputHotSpotHeader("Nehalem5.flp");
        chip->outputHotSpotLayout(HSOut);
        outputHotSpotFooter(HSOut);
        setNameMode(true);
    }
    delete chip;
}